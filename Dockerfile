FROM openjdk:17-jdk

COPY target/inventory-api-0.0.1-SNAPSHOT.jar /app/inventory-api.jar

EXPOSE 8085

CMD["java", "-jar", "/app/inventory-api.jar"]
