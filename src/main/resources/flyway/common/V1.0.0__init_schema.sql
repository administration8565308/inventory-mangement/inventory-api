create table if not exists ENTERPRISE (
                            id SERIAL PRIMARY KEY,
                            CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                            UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                            name VARCHAR(255) NOT NULL,
                            description VARCHAR(255) NOT NULL,
                            address1 VARCHAR(255) NOT NULL,
                            address2 VARCHAR(255),
                            city VARCHAR(255) NOT NULL,
                            code_postal INT NOT NULL,
                            country VARCHAR(255) NOT NULL,
                            code_fiscal VARCHAR(255) NOT NULL,
                            picture VARCHAR(255) NOT NULL,
                            email VARCHAR(255) NOT NULL,
                            phone INT NOT NULL,
                            site_web VARCHAR(255) NOT NULL
);

CREATE TABLE if not exists SALE (
                      id SERIAL PRIMARY KEY,
                      CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL ,
                      UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                      code VARCHAR(255) NOT NULL,
                      date TIMESTAMP NOT NULL,
                      comments VARCHAR(255) NOT NULL
);

CREATE TABLE if not exists PROVIDERS (
                           id SERIAL PRIMARY KEY,
                           CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL ,
                           UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                           first_name VARCHAR(255) NOT NULL,
                           last_name VARCHAR(255) NOT NULL,
                           address1 VARCHAR(255) NOT NULL,
                           address2 VARCHAR(255),
                           city VARCHAR(255) NOT NULL,
                           code_postal INT NOT NULL,
                           country VARCHAR(255) NOT NULL,
                           picture VARCHAR(255) NOT NULL,
                           email VARCHAR(255) NOT NULL,
                           phone INT NOT NULL
);

CREATE TABLE if not exists CUSTOMERS (
                           id SERIAL PRIMARY KEY,
                           CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                           UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                           first_name VARCHAR(255) NOT NULL,
                           last_name VARCHAR(255) NOT NULL,
                           address1 VARCHAR(255) NOT NULL,
                           address2 VARCHAR(255),
                           city VARCHAR(255) NOT NULL,
                           code_postal INT NOT NULL,
                           country VARCHAR(255) NOT NULL,
                           picture VARCHAR(255) NOT NULL,
                           email VARCHAR(255) NOT NULL,
                           phone INT NOT NULL
);

CREATE TABLE if not exists CATEGORIES (
                            id SERIAL PRIMARY KEY,
                            CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                            UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                            code VARCHAR(255) NOT NULL,
                            designation VARCHAR(255) NOT NULL
);

CREATE TABLE if not exists USERS (
                       id SERIAL PRIMARY KEY,
                       CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                       UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                       first_name VARCHAR(255) NOT NULL,
                       last_name VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL,
                       birth_date TIMESTAMP NOT NULL,
                       address1 VARCHAR(255) NOT NULL,
                       address2 VARCHAR(255),
                       city VARCHAR(255) NOT NULL,
                       code_postal INT NOT NULL,
                       country VARCHAR(255) NOT NULL,
                       picture VARCHAR(255) NOT NULL,
                       id_enterprise INTEGER,
                       FOREIGN KEY (id_enterprise) REFERENCES ENTERPRISE (id)
);

CREATE TABLE if not exists ARTICLES (
                          id SERIAL PRIMARY KEY,
                          CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                          UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                          code VARCHAR(255) NOT NULL,
                          designation VARCHAR(255) NOT NULL,
                          unit_price_ht DECIMAL(19, 2) NOT NULL,
                          rate_tax DECIMAL(19, 2) NOT NULL,
                          unit_price_ttc DECIMAL(19, 2) NOT NULL,
                          picture VARCHAR(255) NOT NULL,
                          ID_CATEGORY INTEGER,
                          ID_ENTERPRISE INT,
                          FOREIGN KEY (ID_CATEGORY) REFERENCES CATEGORIES (id),
                          FOREIGN KEY (ID_ENTERPRISE) REFERENCES ENTERPRISE (id)
);

CREATE TABLE if not exists CUSTOMER_ORDER (
                                id SERIAL PRIMARY KEY,
                                CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                                code VARCHAR(255) NOT NULL,
                                date_order TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                state VARCHAR(255) NOT NULL,
                                ID_CUSTOMER INT,
                                FOREIGN KEY (ID_CUSTOMER) REFERENCES CUSTOMERS (id)
);

CREATE TABLE if not exists CUSTOMER_ORDER_LINE (
                                     id SERIAL PRIMARY KEY,
                                     CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                     UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                                     quantity DECIMAL(19, 2) NOT NULL,
                                     unit_price DECIMAL(19, 2) NOT NULL,
                                     ID_ARTICLE INT,
                                     ID_CUSTOMER_ORDER INT,
                                     FOREIGN KEY (ID_ARTICLE) REFERENCES ARTICLES (id),
                                     FOREIGN KEY (ID_CUSTOMER_ORDER) REFERENCES CUSTOMER_ORDER (id)
);

CREATE TABLE if not exists PROVIDER_ORDER (
                                id SERIAL PRIMARY KEY,
                                CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                                code VARCHAR(255) NOT NULL,
                                date_order TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                state VARCHAR(255) NOT NULL,
                                ID_PROVIDER INT,
                                FOREIGN KEY (ID_PROVIDER) REFERENCES PROVIDERS (id)
);

CREATE TABLE if not exists PROVIDER_ORDER_LINE (
                                     id SERIAL PRIMARY KEY,
                                     CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                     UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                                     quantity DECIMAL(19, 2) NOT NULL,
                                     unit_price DECIMAL(19, 2) NOT NULL,
                                     ID_ARTICLE INT,
                                     ID_PROVIDER_ORDER INT,
                                     FOREIGN KEY (ID_ARTICLE) REFERENCES ARTICLES (id),
                                     FOREIGN KEY (ID_PROVIDER_ORDER) REFERENCES PROVIDER_ORDER (id)
);

CREATE TABLE if not exists SALE_LINE (
                           id SERIAL PRIMARY KEY,
                           CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                           UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                           quantity DECIMAL(19, 2) NOT NULL,
                           unit_price DECIMAL(19, 2) NOT NULL,
                           ID_ARTICLE INT,
                           ID_SALE INT,
                           FOREIGN KEY (ID_ARTICLE) REFERENCES ARTICLES (id),
                           FOREIGN KEY (ID_SALE) REFERENCES SALE (id)
);

CREATE TABLE if not exists STOCK_MOVEMENT (
                                id SERIAL PRIMARY KEY,
                                CREATION_DATE TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                UPDATE_DATE TIMESTAMP(6) WITH TIME ZONE,
                                quantity DECIMAL(19, 2) NOT NULL,
                                date TIMESTAMP(6) WITH TIME ZONE NOT NULL,
                                type VARCHAR(255) NOT NULL,
                                source VARCHAR(255) NOT NULL,
                                ID_ARTICLE INT,
                                FOREIGN KEY (ID_ARTICLE) REFERENCES ARTICLES (id)
);