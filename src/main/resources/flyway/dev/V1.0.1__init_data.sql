INSERT INTO ENTERPRISE (id, CREATION_DATE, UPDATE_DATE, name, description, address1, address2, city, code_postal, country, code_fiscal, picture, email, phone, site_web)
VALUES
    (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'SEDONA SOLUTIONS', 'ESN Solution Digital', '11 avenue de la marne', null, 'Paris', 75001, 'France', 'BOI-INT-AEA-20-30', 'https://www.sedona.fr/wp-content/uploads/2023/01/MicrosoftTeams-image-22.png', 'contact@sedona.fr', 0144948990, 'https://www.sedona.fr/');


INSERT INTO USERS (id, CREATION_DATE, UPDATE_DATE, first_name, last_name, email, birth_date, address1, address2, city, code_postal, country, picture, id_enterprise)
VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 'John', 'Doe', 'john.doe@example.com', '1990-01-01T00:00:00.000Z', '123 Main St', NULL, 'New York', 12345, 'USA', 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg', 1);