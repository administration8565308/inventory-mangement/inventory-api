package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.math.BigDecimal


@Entity
@Table(name = "SALE_LINE")
@EqualsAndHashCode(callSuper = true)
class SaleLineEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sale_line_id_seq")
    @SequenceGenerator(
        name = "sale_line_id_seq",
        sequenceName = "sale_line_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var quantity: BigDecimal?= null

    var unitPrice: BigDecimal?= null

    @ManyToOne
    @JoinColumn(name = "ID_ARTICLE")
    var article: ArticleEntity?= null

    @ManyToOne
    @JoinColumn(name = "ID_SALE")
    var sale: SaleEntity?= null
}