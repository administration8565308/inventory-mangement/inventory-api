package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode

@Entity
@Table(name = "ENTERPRISE")
@EqualsAndHashCode(callSuper = true)
class EnterpriseEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "enterprise_id_seq")
    @SequenceGenerator(
        name = "enterprise_id_seq",
        sequenceName = "enterprise_id_seq",
        allocationSize = 1
    )
    var id: Int? = null

    var name: String? = null

    var description: String? = null

    @Embedded
    var address: AddressModel?= null

    var codeFiscal: String? = null

    var picture: String? = null

    var email: String? = null

    var phone: Int = 0

    var siteWeb: String? = null

    @OneToMany(mappedBy = "enterprise")
    var usersList: MutableList<UserEntity>?= mutableListOf()

    @OneToMany(mappedBy = "enterprise")
    var articleList: MutableList<ArticleEntity>?= mutableListOf()
}