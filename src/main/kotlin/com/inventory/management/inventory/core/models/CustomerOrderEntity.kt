package com.inventory.management.inventory.core.models

import com.inventory.management.inventory.core.enums.StateOrder
import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.time.ZonedDateTime

@Entity
@Table(name = "CUSTOMER_ORDER")
@EqualsAndHashCode(callSuper = true)
class CustomerOrderEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_order_id_seq")
    @SequenceGenerator(
        name = "customer_order_id_seq",
        sequenceName = "customer_order_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var code: String ?= null

    var dateOrder: ZonedDateTime ?= null

    @Enumerated(EnumType.STRING)
    var state: StateOrder ?= null

    @ManyToOne
    @JoinColumn(name = "ID_CUSTOMER")
    var customer: CustomerEntity ?= null

    @OneToMany(mappedBy = "customerOrder")
    var customerOrderLineList: MutableList<CustomerOrderLineEntity> ?= null
}