package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode

@Entity
@Table(name = "PROVIDERS")
@EqualsAndHashCode(callSuper = true)
class ProviderEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "providers_id_seq")
    @SequenceGenerator(
        name = "providers_id_seq",
        sequenceName = "providers_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var firstName: String?= null

    var lastName: String?= null

    @Embedded
    var address: AddressModel?= null

    var picture: String?= null

    var email: String?= null

    var phone: Int = 0

    @OneToMany(mappedBy = "provider")
    var providerOrderList: MutableList<ProviderOrderEntity>?= mutableListOf()
}