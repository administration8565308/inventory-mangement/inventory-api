package com.inventory.management.inventory.core.models

import jakarta.persistence.Embeddable
import lombok.EqualsAndHashCode
import java.io.Serializable

@EqualsAndHashCode
@Embeddable
class AddressModel: Serializable {

    var address1: String?= null

    var address2: String?= null

    var city: String?= null

    var codePostal: Int = 0

    var country: String?= null
}