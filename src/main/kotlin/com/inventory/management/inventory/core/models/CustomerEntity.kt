package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode

@Entity
@Table(name = "CUSTOMERS")
@EqualsAndHashCode(callSuper = true)
class CustomerEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customers_id_seq")
    @SequenceGenerator(
        name = "customers_id_seq",
        sequenceName = "customers_id_seq",
        allocationSize = 1
    )
    var id: Int? = null

    var firstName: String?= null

    var lastName: String?= null

    @Embedded // Champs composé capable d'être utiliser dans les autres entités
    var address: AddressModel?= null

    var picture: String?= null

    var email: String?= null

    var phone: Int = 0

    @OneToMany(mappedBy = "customer")
    var customerOrderList: MutableList<CustomerOrderEntity> = mutableListOf()
}