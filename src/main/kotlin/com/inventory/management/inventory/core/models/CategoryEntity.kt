package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode

@Entity
@Table(name = "CATEGORIES")
@EqualsAndHashCode(callSuper = true)
class CategoryEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categories_id_seq")
    @SequenceGenerator(
        name = "categories_id_seq",
        sequenceName = "categories_id_seq",
        allocationSize = 1
    )
    var id: Int? = null

    var code: String ? = null

    var designation: String ? = null

    @OneToMany(mappedBy = "category")
    var articleList: MutableList<ArticleEntity> = mutableListOf()
}