package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.math.BigDecimal

@Entity
@Table(name = "ARTICLES")
@EqualsAndHashCode(callSuper = true)
class ArticleEntity: AbstractModel(){
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "articles_id_seq")
    @SequenceGenerator(
        name = "articles_id_seq",
        sequenceName = "articles_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var code: String?= null

    var designation: String?= null

    var unitPriceHt: BigDecimal? = null

    var rateTax: BigDecimal? = null

    var unitPriceTtc: BigDecimal? = null

    var picture: String?= null

    @ManyToOne
    @JoinColumn(name = "ID_CATEGORY")
    var category: CategoryEntity? = null

    @ManyToOne
    @JoinColumn(name = "ID_ENTERPRISE")
    var enterprise: EnterpriseEntity? = null

    @OneToMany(mappedBy = "article")
    var stockMovementList: MutableList<StockMovementEntity> = mutableListOf()

    @OneToMany(mappedBy = "article")
    var customerOrderLineList: MutableList<CustomerOrderLineEntity> = mutableListOf()

    @OneToMany(mappedBy = "article")
    var providerOrderLineList: MutableList<ProviderOrderLineEntity> = mutableListOf()

    @OneToMany(mappedBy = "article")
    var saleLineList: MutableList<SaleLineEntity> = mutableListOf()

}