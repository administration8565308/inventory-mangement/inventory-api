package com.inventory.management.inventory.core.enums

enum class SourceStockMovement {
    CUSTOMER_ORDER, PROVIDER_ORDER, SALE
}