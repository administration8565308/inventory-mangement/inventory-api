package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.math.BigDecimal

@Entity
@Table(name = "PROVIDER_ORDER_LINE")
@EqualsAndHashCode(callSuper = true)
class ProviderOrderLineEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_order_line_id_seq")
    @SequenceGenerator(
        name = "provider_order_line_id_seq",
        sequenceName = "provider_order_line_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var quantity: BigDecimal?= null

    var unitPrice: BigDecimal?= null

    @ManyToOne
    @JoinColumn(name = "ID_ARTICLE")
    var article: ArticleEntity?= null

    @ManyToOne
    @JoinColumn(name = "ID_PROVIDER_ORDER")
    var providerOrder: ProviderOrderEntity?= null
}