package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.time.ZonedDateTime

@Entity
@Table(name = "SALE")
@EqualsAndHashCode(callSuper = true)
class SaleEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sale_id_seq")
    @SequenceGenerator(
        name = "sale_id_seq",
        sequenceName = "sale_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var code: String?= null

    var date: ZonedDateTime?= null

    var comments: String?= null

    @OneToMany(mappedBy = "sale")
    var saleLineList: MutableList<SaleLineEntity> = mutableListOf()
}