package com.inventory.management.inventory.core.models

import com.inventory.management.inventory.core.enums.SourceStockMovement
import com.inventory.management.inventory.core.enums.TypeMoveStock
import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.math.BigDecimal
import java.time.ZonedDateTime

@Entity
@Table(name = "STOCK_MOVEMENT")
@EqualsAndHashCode(callSuper = true)
class StockMovementEntity : AbstractModel(){
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stock_movement_id_seq")
    @SequenceGenerator(
        name = "stock_movement_id_seq",
        sequenceName = "stock_movement_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var quantity: BigDecimal?= null

    var date: ZonedDateTime?= null

    @Enumerated(EnumType.STRING)
    var type: TypeMoveStock?= null

    @Enumerated(EnumType.STRING)
    var source: SourceStockMovement?= null

    @ManyToOne
    @JoinColumn(name = "ID_ARTICLE")
    var article: ArticleEntity?= null

}