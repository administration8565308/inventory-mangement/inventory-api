package com.inventory.management.inventory.core.enums

enum class StateOrder {
    ON_PROGRESS, VALIDATE, DELIVERED
}