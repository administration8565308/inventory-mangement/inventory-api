package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.io.Serializable
import java.time.ZonedDateTime

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class AbstractModel : Serializable{
    @CreatedDate
    @Column(name = "CREATION_DATE", nullable = false, updatable = false)
    var creationTime: ZonedDateTime?= null

    @LastModifiedDate
    @Column(name = "UPDATE_DATE")
    var lastModifiedTime: ZonedDateTime?= null
}