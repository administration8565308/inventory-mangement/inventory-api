package com.inventory.management.inventory.core.enums

enum class TypeMoveStock {
    ENTRANCE, EXIT, CORRECTION_POSITIVE, CORRECTION_NEGATIVE
}