package com.inventory.management.inventory.core.utils

object UtilsConstant {

    /**
     *  ERROR MESSAGE AFTER THROW EXCEPTION
     */

    const val OBJECT_EXCEPTION = "Object not valid exception has occurred"
    const val USER_EXITS_EXCEPTION = "A user already exists with the provided email"
    const val BAD_CREDENTIAL_EXCEPTION = "Your email and / or password is incorrect"
    const val ARTICLE_NOT_FOUND = "Nothing article has been found with ID ="
    const val CATEGORY_NOT_FOUND = "Nothing category has been found with ID ="


    /**
     *  CUSTOM NOT NUL FUNCTION
     */

    fun <T> notNull(obj: T?): Boolean = obj != null

}