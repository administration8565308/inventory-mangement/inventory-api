package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.math.BigDecimal

@Entity
@Table(name = "CUSTOMER_ORDER_LINE")
@EqualsAndHashCode(callSuper = true)
class CustomerOrderLineEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_order_line_id_seq")
    @SequenceGenerator(
        name = "customer_order_line_id_seq",
        sequenceName = "customer_order_line_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var quantity: BigDecimal?= null

    var unitPrice: BigDecimal?= null

    @ManyToOne
    @JoinColumn(name = "ID_ARTICLE")
    var article: ArticleEntity ?= null

    @ManyToOne
    @JoinColumn(name = "ID_CUSTOMER_ORDER")
    var customerOrder: CustomerOrderEntity ?= null
}