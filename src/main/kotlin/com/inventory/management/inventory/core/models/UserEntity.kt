package com.inventory.management.inventory.core.models

import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.time.ZonedDateTime

@Entity
@Table(name = "USERS")
@EqualsAndHashCode(callSuper = true)
class UserEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    @SequenceGenerator(
        name = "users_id_seq",
        sequenceName = "users_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var firstName: String?= null

    var lastName: String?= null

    var email: String?= null

    var birthDate: ZonedDateTime?= null

    @Embedded
    var address: AddressModel?= null

    var picture: String?= null

    @ManyToOne
    @JoinColumn(name = "id_enterprise")
    var enterprise: EnterpriseEntity?= null

}