package com.inventory.management.inventory.core.models

import com.inventory.management.inventory.core.enums.StateOrder
import jakarta.persistence.*
import lombok.EqualsAndHashCode
import java.time.ZonedDateTime

@Entity
@Table(name = "PROVIDER_ORDER")
@EqualsAndHashCode(callSuper = true)
class ProviderOrderEntity: AbstractModel() {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_order_id_seq")
    @SequenceGenerator(
        name = "provider_order_id_seq",
        sequenceName = "provider_order_id_seq",
        allocationSize = 1
    )
    val id: Int? = null

    var code: String?= null

    var dateOrder: ZonedDateTime?= null

    @Enumerated(EnumType.STRING)
    var state: StateOrder?= null

    @ManyToOne
    @JoinColumn(name = "ID_PROVIDER")
    var provider: ProviderEntity?= null

    @OneToMany(mappedBy = "providerOrder")
    var providerOrderLineList: MutableList<ProviderOrderLineEntity>?= mutableListOf()
}