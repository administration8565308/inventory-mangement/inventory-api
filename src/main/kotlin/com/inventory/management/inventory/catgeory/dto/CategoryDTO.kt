package com.inventory.management.inventory.catgeory.dto

import java.time.ZonedDateTime

class CategoryDTO {
     var id: Int? = null
     var code: String?= null
     var designation: String?= null
     var creationTime: ZonedDateTime? = null
     var lastModifiedTime: ZonedDateTime? = null
}