package com.inventory.management.inventory.catgeory.repository

import com.inventory.management.inventory.core.models.CategoryEntity
import org.springframework.data.jpa.repository.JpaRepository

interface CategoryRepository: JpaRepository<CategoryEntity, Int> {
}