package com.inventory.management.inventory.catgeory.mapper

import com.inventory.management.inventory.catgeory.dto.CategoryDTO
import com.inventory.management.inventory.core.models.CategoryEntity
import java.time.ZonedDateTime

class CategoryMapper {
    fun toCategoryEntity(categoryDTO: CategoryDTO): CategoryEntity = CategoryEntity().also {
        it.code = categoryDTO.code
        it.designation = categoryDTO.designation
        it.creationTime = ZonedDateTime.now()
    }

    fun fromCategoryDTO(categoryEntity: CategoryEntity): CategoryDTO = CategoryDTO().also {
        it.id = categoryEntity.id
        it.code = categoryEntity.code
        it.designation = categoryEntity.designation
        it.creationTime = categoryEntity.creationTime
        it.lastModifiedTime = categoryEntity.lastModifiedTime
    }
}