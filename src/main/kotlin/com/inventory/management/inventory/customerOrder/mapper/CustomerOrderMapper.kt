package com.inventory.management.inventory.customerOrder.mapper

import com.inventory.management.inventory.core.models.CustomerOrderEntity
import com.inventory.management.inventory.customer.mapper.CustomerMapper
import com.inventory.management.inventory.customerOrder.dto.request.CustomerOrderRequestDTO
import com.inventory.management.inventory.customerOrder.dto.response.CustomerOrderResponseDTO
import java.time.ZonedDateTime

class CustomerOrderMapper(
    private val customerMapper: CustomerMapper
) {

    fun toCustomerOrderEntity(customerOrderRequestDTO: CustomerOrderRequestDTO): CustomerOrderEntity =
        CustomerOrderEntity().also {
            it.code = customerOrderRequestDTO.codeOrder
            it.dateOrder = customerOrderRequestDTO.dateOrder
            it.state = customerOrderRequestDTO.stateOrder
            it.creationTime = ZonedDateTime.now()
        }


    fun fromCustomerOrderDTO(customerOrderEntity: CustomerOrderEntity): CustomerOrderResponseDTO =
        CustomerOrderResponseDTO().also {
            it.id = customerOrderEntity.id
            it.codeOrder = customerOrderEntity.code
            it.dateOrder = customerOrderEntity.dateOrder
            it.stateOrder = customerOrderEntity.state
            it.customerDTO = customerOrderEntity.customer?.let { customerOrder ->
                customerMapper.fromCustomerDTO(customerOrder)
            }
            it.creationTime = customerOrderEntity.creationTime
            it.lastModifiedTime = customerOrderEntity.lastModifiedTime
        }
}