package com.inventory.management.inventory.customerOrder.repository

import com.inventory.management.inventory.core.models.CustomerOrderEntity
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerOrderRepository: JpaRepository<CustomerOrderEntity, Int> {
}