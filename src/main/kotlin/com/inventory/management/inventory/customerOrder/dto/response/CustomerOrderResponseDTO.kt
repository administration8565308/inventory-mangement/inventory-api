package com.inventory.management.inventory.customerOrder.dto.response

import com.inventory.management.inventory.core.enums.StateOrder
import com.inventory.management.inventory.customer.dto.CustomerDTO
import lombok.Data
import java.time.ZonedDateTime

@Data
class CustomerOrderResponseDTO {
    var id: Int? = null
    var codeOrder: String?= null
    var dateOrder: ZonedDateTime?= null
    var stateOrder: StateOrder?= null
    var customerDTO: CustomerDTO?= null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}