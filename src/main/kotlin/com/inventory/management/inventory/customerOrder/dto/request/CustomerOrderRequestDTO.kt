package com.inventory.management.inventory.customerOrder.dto.request

import com.inventory.management.inventory.core.enums.StateOrder
import lombok.Data
import java.time.ZonedDateTime

@Data
class CustomerOrderRequestDTO {
    var codeOrder: String?= null
    var dateOrder: ZonedDateTime?= null
    var stateOrder: StateOrder?= null
}