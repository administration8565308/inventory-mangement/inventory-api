package com.inventory.management.inventory.user.dto.request

import com.inventory.management.inventory.AddressDTO
import lombok.Data
import java.time.ZonedDateTime

class UserRequestDTO {
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var birthDate: ZonedDateTime? = null
    var address: AddressDTO? = null
    var picture: String? = null
}