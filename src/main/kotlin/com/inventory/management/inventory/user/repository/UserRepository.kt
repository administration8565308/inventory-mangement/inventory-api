package com.inventory.management.inventory.user.repository

import com.inventory.management.inventory.core.models.UserEntity
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<UserEntity, Int> {
}