package com.inventory.management.inventory.user.mapper

import com.inventory.management.inventory.AddressMapper
import com.inventory.management.inventory.core.models.UserEntity
import com.inventory.management.inventory.enterprise.mapper.EnterpriseMapper
import com.inventory.management.inventory.user.dto.request.UserRequestDTO
import com.inventory.management.inventory.user.dto.response.UserResponseDTO
import java.time.ZonedDateTime

class UserMapper (
    private val enterpriseMapper: EnterpriseMapper,
    private val addressMapper: AddressMapper
) {

    fun toUserEntity(userRequestDTO: UserRequestDTO): UserEntity = UserEntity().also {
        it.firstName = userRequestDTO.firstName
        it.lastName = userRequestDTO.lastName
        it.email = userRequestDTO.email
        it.picture = userRequestDTO.picture
        it.birthDate = userRequestDTO.birthDate
        it.creationTime = ZonedDateTime.now()
    }

    fun fromUserDTO(userEntity: UserEntity): UserResponseDTO = UserResponseDTO().also {
        it.id = userEntity.id
        it.firstName = userEntity.firstName
        it.lastName = userEntity.lastName
        it.email = userEntity.email
        it.picture = userEntity.picture
        it.birthDate = userEntity.birthDate
        it.enterpriseDTO = userEntity.enterprise?.let { enterprise -> enterpriseMapper.fromEnterpriseDTO(enterprise) }
        it.addressDTO = userEntity.address?.let { address -> addressMapper.fromAddressModel(address)}
        it.creationTime = userEntity.creationTime
        it.lastModifiedTime = userEntity.lastModifiedTime
    }
}