package com.inventory.management.inventory.user.dto.response

import com.inventory.management.inventory.AddressDTO
import com.inventory.management.inventory.enterprise.dto.EnterpriseDTO
import lombok.Data
import java.time.ZonedDateTime

class UserResponseDTO {
    var id: Int? = null
    var firstName: String? = null
    var lastName: String? = null
    var email: String? = null
    var birthDate: ZonedDateTime? = null
    var addressDTO: AddressDTO? = null
    var picture: String? = null
    var enterpriseDTO: EnterpriseDTO? = null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}