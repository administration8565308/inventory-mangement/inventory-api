package com.inventory.management.inventory.customer.mapper

import com.inventory.management.inventory.AddressMapper
import com.inventory.management.inventory.core.models.CustomerEntity
import com.inventory.management.inventory.customer.dto.CustomerDTO
import java.time.ZonedDateTime

class CustomerMapper (
    private val addressMapper: AddressMapper
) {
    fun toCustomerEntity(customerDTO: CustomerDTO): CustomerEntity = CustomerEntity().also {
        it.firstName = customerDTO.firstName
        it.lastName = customerDTO.lastName
        it.email = customerDTO.email
        it.phone = customerDTO.phone
        it.picture = customerDTO.picture
        it.address = customerDTO.addressDTO?.let { address -> addressMapper.toAddressModel(address) }
        it.creationTime = ZonedDateTime.now()
    }

    fun fromCustomerDTO (customerEntity: CustomerEntity): CustomerDTO = CustomerDTO().also {
        it.id = customerEntity.id
        it.firstName = customerEntity.firstName
        it.lastName = customerEntity.lastName
        it.email = customerEntity.email
        it.phone = customerEntity.phone
        it.picture = customerEntity.picture
        it.addressDTO = customerEntity.address?.let { address -> addressMapper.fromAddressModel(address) }
        it.creationTime = customerEntity.creationTime
        it.lastModifiedTime = customerEntity.lastModifiedTime
    }
}