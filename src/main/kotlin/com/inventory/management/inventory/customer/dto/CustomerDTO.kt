package com.inventory.management.inventory.customer.dto

import com.inventory.management.inventory.AddressDTO
import lombok.Data
import java.time.ZonedDateTime

@Data
class CustomerDTO {
    var id: Int? = null
    var firstName: String?= null
    var lastName: String?= null
    var email: String?= null
    var addressDTO: AddressDTO?= null
    var phone: Int = 0
    var picture: String ?= null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}