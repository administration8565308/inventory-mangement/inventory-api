package com.inventory.management.inventory.customer.repository

import com.inventory.management.inventory.core.models.CustomerEntity
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerRepository: JpaRepository<CustomerEntity, Int> {
}