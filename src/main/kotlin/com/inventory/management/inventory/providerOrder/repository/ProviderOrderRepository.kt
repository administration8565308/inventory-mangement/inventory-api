package com.inventory.management.inventory.providerOrder.repository

import com.inventory.management.inventory.core.models.ProviderOrderEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ProviderOrderRepository: JpaRepository<ProviderOrderEntity, Int> {
}