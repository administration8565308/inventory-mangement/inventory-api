package com.inventory.management.inventory.providerOrder.dto.request

import com.inventory.management.inventory.core.enums.StateOrder
import java.time.ZonedDateTime

class ProviderOrderRequestDTO {
    var code: String?= null
    var dateOrdering: ZonedDateTime?= null
    var stateOrder: StateOrder?= null
}