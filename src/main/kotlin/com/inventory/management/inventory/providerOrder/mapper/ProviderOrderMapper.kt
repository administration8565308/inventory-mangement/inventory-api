package com.inventory.management.inventory.providerOrder.mapper

import com.inventory.management.inventory.core.models.ProviderOrderEntity
import com.inventory.management.inventory.provider.mapper.ProviderMapper
import com.inventory.management.inventory.providerOrder.dto.request.ProviderOrderRequestDTO
import com.inventory.management.inventory.providerOrder.dto.response.ProviderOrderResponseDTO
import java.time.ZonedDateTime

class ProviderOrderMapper(
    private val providerMapper: ProviderMapper
) {

    fun toProviderOrderEntity(providerOrderDTO: ProviderOrderRequestDTO): ProviderOrderEntity =
        ProviderOrderEntity().also {
            it.code = providerOrderDTO.code
            it.dateOrder = providerOrderDTO.dateOrdering
            it.state = providerOrderDTO.stateOrder
            it.creationTime = ZonedDateTime.now()
        }


    fun fromProviderOrderDTO(providerOrderEntity: ProviderOrderEntity): ProviderOrderResponseDTO =
        ProviderOrderResponseDTO().also {
            it.id = providerOrderEntity.id
            it.code = providerOrderEntity.code
            it.dateOrdering = providerOrderEntity.dateOrder
            it.stateOrder = providerOrderEntity.state
            it.providerDto = providerOrderEntity.provider?.let { provider -> providerMapper.fromProviderDTO(provider) }
            it.creationTime = providerOrderEntity.creationTime
            it.lastModifiedTime = providerOrderEntity.lastModifiedTime
        }
}