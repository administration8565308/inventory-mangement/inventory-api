package com.inventory.management.inventory.providerOrder.dto.response

import com.inventory.management.inventory.core.enums.StateOrder
import com.inventory.management.inventory.provider.dto.ProviderDTO
import java.time.ZonedDateTime

class ProviderOrderResponseDTO {
    var id: Int? = null
    var code: String?= null
    var dateOrdering: ZonedDateTime?= null
    var stateOrder: StateOrder?= null
    var providerDto: ProviderDTO?= null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}