package com.inventory.management.inventory.enterprise.repository

import com.inventory.management.inventory.core.models.EnterpriseEntity
import com.inventory.management.inventory.core.models.SaleEntity
import org.springframework.data.jpa.repository.JpaRepository

interface EnterpriseRepository: JpaRepository<EnterpriseEntity, Int> {
}