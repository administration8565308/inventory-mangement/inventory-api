package com.inventory.management.inventory.enterprise.dto

import com.inventory.management.inventory.AddressDTO
import java.time.ZonedDateTime

class EnterpriseDTO {
    var id: Int? = null
    var name: String?= null
    var description: String?= null
    var address: AddressDTO?= null
    var codeFiscal: String?= null
    var picture: String?= null
    var email: String?= null
    var phone: Int = 0
    var siteWeb: String?= null
    var creationTime: ZonedDateTime?= null
    var lastModifiedTime: ZonedDateTime?= null
}