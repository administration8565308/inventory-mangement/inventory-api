package com.inventory.management.inventory.enterprise.mapper

import com.inventory.management.inventory.core.models.EnterpriseEntity
import com.inventory.management.inventory.enterprise.dto.EnterpriseDTO
import java.time.ZonedDateTime

class EnterpriseMapper {

    fun toEnterpriseEntity(enterpriseDTO: EnterpriseDTO): EnterpriseEntity = EnterpriseEntity().also {
        it.name = enterpriseDTO.name
        it.description = enterpriseDTO.description
        it.codeFiscal = enterpriseDTO.codeFiscal
        it.picture = enterpriseDTO.picture
        it.email = enterpriseDTO.email
        it.phone = enterpriseDTO.phone
        it.creationTime = ZonedDateTime.now()
    }

    fun fromEnterpriseDTO(enterpriseEntity: EnterpriseEntity): EnterpriseDTO = EnterpriseDTO().also {
        it.id = enterpriseEntity.id
        it.name = enterpriseEntity.name
        it.description = enterpriseEntity.description
        it.codeFiscal = enterpriseEntity.codeFiscal
        it.picture = enterpriseEntity.picture
        it.email = enterpriseEntity.email
        it.phone = enterpriseEntity.phone
        it.siteWeb = enterpriseEntity.siteWeb
        it.creationTime = enterpriseEntity.creationTime
        it.lastModifiedTime = enterpriseEntity.lastModifiedTime
    }
}