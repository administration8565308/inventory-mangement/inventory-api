package com.inventory.management.inventory

import com.inventory.management.inventory.core.models.AddressModel

class AddressMapper {

    fun toAddressModel(addressDTO: AddressDTO): AddressModel = AddressModel().also {
        it.address1 = addressDTO.address1
        it.address2 = addressDTO.address2
        it.city = addressDTO.city
        it.codePostal = addressDTO.codePostal
        it.country = addressDTO.country
    }

    fun fromAddressModel(addressModel: AddressModel): AddressDTO = AddressDTO().also {
        it.address1 = addressModel.address1
        it.address2 = addressModel.address2
        it.city = addressModel.city
        it.codePostal = addressModel.codePostal
        it.country = addressModel.country
    }
}