package com.inventory.management.inventory.article.mapper

import com.inventory.management.inventory.article.dto.request.ArticleRequestDTO
import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO
import com.inventory.management.inventory.catgeory.mapper.CategoryMapper
import com.inventory.management.inventory.core.models.ArticleEntity
import com.inventory.management.inventory.enterprise.mapper.EnterpriseMapper
import java.time.ZonedDateTime

class ArticleMapper(
    private val enterpriseMapper: EnterpriseMapper,
    private val categoryMapper: CategoryMapper
) {
    fun toArticleEntity(articleRequestDTO: ArticleRequestDTO): ArticleEntity = ArticleEntity().also {
        it.code = articleRequestDTO.code
        it.designation = articleRequestDTO.designation
        it.unitPriceHt = articleRequestDTO.unitPriceHt
        it.rateTax = articleRequestDTO.rateTax
        it.unitPriceTtc = articleRequestDTO.unitPriceTtc
        it.picture = articleRequestDTO.picture
        it.creationTime = ZonedDateTime.now()
    }

    fun fromArticleDto(articleEntity: ArticleEntity): ArticleResponseDTO = ArticleResponseDTO().also {
        it.id = articleEntity.id
        it.code = articleEntity.code
        it.designation = articleEntity.designation
        it.unitPriceHt = articleEntity.unitPriceHt
        it.rateTax = articleEntity.rateTax
        it.unitPriceTtc = articleEntity.unitPriceTtc
        it.picture = articleEntity.picture
        it.enterpriseDTO = articleEntity.enterprise?.let { enterprise -> enterpriseMapper.fromEnterpriseDTO(enterprise) }
        it.categoryDTO = articleEntity.category?.let { category -> categoryMapper.fromCategoryDTO(category) }
        it.creationTime = articleEntity.creationTime
        it.lastModifiedTime = articleEntity.lastModifiedTime
    }
}