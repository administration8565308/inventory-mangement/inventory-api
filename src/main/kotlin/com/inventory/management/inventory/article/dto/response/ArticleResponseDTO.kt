package com.inventory.management.inventory.article.dto.response

import com.inventory.management.inventory.catgeory.dto.CategoryDTO
import com.inventory.management.inventory.enterprise.dto.EnterpriseDTO
import java.math.BigDecimal
import java.time.ZonedDateTime

class ArticleResponseDTO {
    var id: Int? = null
    var code: String? = null
    var designation: String? = null
    var unitPriceHt: BigDecimal? = null
    var rateTax: BigDecimal? = null
    var unitPriceTtc: BigDecimal? = null
    var picture: String? = null
    var categoryDTO: CategoryDTO? = null
    var enterpriseDTO: EnterpriseDTO? = null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}