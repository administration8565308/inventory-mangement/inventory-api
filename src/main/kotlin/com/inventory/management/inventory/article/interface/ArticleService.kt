package com.inventory.management.inventory.article.`interface`

import com.inventory.management.inventory.article.dto.request.ArticleRequestDTO
import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO

interface ArticleService {
    fun saveArticle(articleDTO: ArticleRequestDTO): ArticleResponseDTO
    fun updateArticle(articleDTO: ArticleRequestDTO): ArticleResponseDTO
    fun getArticle(id: Int): ArticleResponseDTO
    fun getCodeArticle(code: String): ArticleResponseDTO
    fun listArticle(): List<ArticleResponseDTO>
    fun deleteArticle(id: Int): Void
}