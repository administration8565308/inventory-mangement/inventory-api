package com.inventory.management.inventory.article.dto.request

import java.math.BigDecimal

class ArticleRequestDTO {
    var code: String? = null
    var designation: String? = null
    var unitPriceHt: BigDecimal? = null
    var rateTax: BigDecimal? = null
    var unitPriceTtc: BigDecimal? = null
    var picture: String? = null
}