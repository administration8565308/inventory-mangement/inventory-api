package com.inventory.management.inventory.article.repository

import com.inventory.management.inventory.core.models.ArticleEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ArticleRepository : JpaRepository<ArticleEntity, Int> {
}