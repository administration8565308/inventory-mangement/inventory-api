package com.inventory.management.inventory.saleLine.repository

import com.inventory.management.inventory.core.models.SaleLineEntity
import org.springframework.data.jpa.repository.JpaRepository

interface SaleLineRepository: JpaRepository<SaleLineEntity, Int> {
}