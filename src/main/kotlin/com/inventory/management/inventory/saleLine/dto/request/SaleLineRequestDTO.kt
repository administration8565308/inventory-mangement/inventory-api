package com.inventory.management.inventory.saleLine.dto.request

import java.math.BigDecimal

class SaleLineRequestDTO {
    var quantity: BigDecimal? = null
    var unitPrice: BigDecimal? = null
}