package com.inventory.management.inventory.saleLine.dto.response

import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO
import com.inventory.management.inventory.sale.dto.SaleDTO
import java.math.BigDecimal
import java.time.ZonedDateTime

class SaleLineResponseDTO {
    var id: Int? = null
    var quantity: BigDecimal? = null
    var unitPrice: BigDecimal? = null
    var saleDTO: SaleDTO? = null
    var articleDTO: ArticleResponseDTO? = null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}