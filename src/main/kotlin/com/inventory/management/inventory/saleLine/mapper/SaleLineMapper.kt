package com.inventory.management.inventory.saleLine.mapper

import com.inventory.management.inventory.article.mapper.ArticleMapper
import com.inventory.management.inventory.core.models.SaleLineEntity
import com.inventory.management.inventory.sale.mapper.SaleMapper
import com.inventory.management.inventory.saleLine.dto.request.SaleLineRequestDTO
import com.inventory.management.inventory.saleLine.dto.response.SaleLineResponseDTO
import java.time.ZonedDateTime

class SaleLineMapper (
    private val articleMapper: ArticleMapper,
    private val saleMapper: SaleMapper
) {

    fun toSaleLineEntity(saleLineRequestDTO: SaleLineRequestDTO): SaleLineEntity = SaleLineEntity().also {
        it.quantity = saleLineRequestDTO.quantity
        it.unitPrice = saleLineRequestDTO.unitPrice
        it.creationTime = ZonedDateTime.now()
    }

    fun fromSaleLineDTO(saleLineEntity: SaleLineEntity): SaleLineResponseDTO = SaleLineResponseDTO().also {
        it.id = saleLineEntity.id
        it.quantity = saleLineEntity.quantity
        it.unitPrice = saleLineEntity.unitPrice
        it.saleDTO = saleLineEntity.sale?.let { sale -> saleMapper.fromSaleDTO(sale) }
        it.articleDTO = saleLineEntity.article?.let { article -> articleMapper.fromArticleDto(article) }
        it.creationTime = saleLineEntity.creationTime
    }
}