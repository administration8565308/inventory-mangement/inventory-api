package com.inventory.management.inventory.customerOrderLine.dto.response

import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO
import com.inventory.management.inventory.customerOrder.dto.response.CustomerOrderResponseDTO
import lombok.Data
import java.math.BigDecimal
import java.time.ZonedDateTime

class CustomerOrderLineResponseDTO {
    var id: Int? = null
    var quantity: BigDecimal?= null
    var unitPrice: BigDecimal?= null
    var articleDTO: ArticleResponseDTO?= null
    var customerOrderDTO: CustomerOrderResponseDTO?= null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}