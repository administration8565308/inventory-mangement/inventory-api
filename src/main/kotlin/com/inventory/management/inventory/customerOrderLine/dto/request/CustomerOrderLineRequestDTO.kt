package com.inventory.management.inventory.customerOrderLine.dto.request

import lombok.Data
import java.math.BigDecimal

class CustomerOrderLineRequestDTO {
    var quantity: BigDecimal?= null
    var unitPrice: BigDecimal?= null
}