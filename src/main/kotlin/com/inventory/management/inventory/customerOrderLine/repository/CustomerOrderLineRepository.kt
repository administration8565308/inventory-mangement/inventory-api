package com.inventory.management.inventory.customerOrderLine.repository

import com.inventory.management.inventory.core.models.CustomerOrderLineEntity
import org.springframework.data.jpa.repository.JpaRepository

interface CustomerOrderLineRepository: JpaRepository<CustomerOrderLineEntity, Int> {
}