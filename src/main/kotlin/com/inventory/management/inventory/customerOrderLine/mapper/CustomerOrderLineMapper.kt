package com.inventory.management.inventory.customerOrderLine.mapper

import com.inventory.management.inventory.article.mapper.ArticleMapper
import com.inventory.management.inventory.core.models.CustomerOrderLineEntity
import com.inventory.management.inventory.customerOrder.mapper.CustomerOrderMapper
import com.inventory.management.inventory.customerOrderLine.dto.request.CustomerOrderLineRequestDTO
import com.inventory.management.inventory.customerOrderLine.dto.response.CustomerOrderLineResponseDTO
import java.time.ZonedDateTime

class CustomerOrderLineMapper(
    private val customerOrderMapper: CustomerOrderMapper,
    private val articleMapper: ArticleMapper
) {

    fun toCustomerOrderLineEntity(customerOrderLineRequestDTO: CustomerOrderLineRequestDTO): CustomerOrderLineEntity =
        CustomerOrderLineEntity().also {
            it.quantity = customerOrderLineRequestDTO.quantity
            it.unitPrice = customerOrderLineRequestDTO.unitPrice
            it.creationTime = ZonedDateTime.now()
        }

    fun fromCustomerOrderLineDTO(customerOrderLineEntity: CustomerOrderLineEntity): CustomerOrderLineResponseDTO =
        CustomerOrderLineResponseDTO().also {
            it.id = customerOrderLineEntity.id
            it.quantity = customerOrderLineEntity.quantity
            it.unitPrice = customerOrderLineEntity.unitPrice
            it.customerOrderDTO = customerOrderLineEntity.customerOrder?.let { customerOrder ->
                customerOrderMapper.fromCustomerOrderDTO(
                    customerOrder
                )
            }
            it.articleDTO = customerOrderLineEntity.article?.let { article ->
                articleMapper.fromArticleDto(
                    article
                )
            }
            it.creationTime = customerOrderLineEntity.creationTime
            it.lastModifiedTime = customerOrderLineEntity.lastModifiedTime
        }
}