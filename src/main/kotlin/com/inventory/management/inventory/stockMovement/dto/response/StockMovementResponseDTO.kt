package com.inventory.management.inventory.stockMovement.dto.response

import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO
import com.inventory.management.inventory.core.enums.SourceStockMovement
import com.inventory.management.inventory.core.enums.TypeMoveStock
import java.math.BigDecimal
import java.time.ZonedDateTime

class StockMovementResponseDTO {
    var id: Int? = null
    var date: ZonedDateTime? = null
    var quantity: BigDecimal? = null
    var typeMoveStock: TypeMoveStock? = null
    var sourceStockMovement: SourceStockMovement? = null
    var articleDTO: ArticleResponseDTO? = null
    var creationTime: ZonedDateTime?= null
    var lastModifiedTime: ZonedDateTime?= null
}