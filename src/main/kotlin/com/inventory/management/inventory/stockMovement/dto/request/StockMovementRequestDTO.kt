package com.inventory.management.inventory.stockMovement.dto.request

import com.inventory.management.inventory.core.enums.SourceStockMovement
import com.inventory.management.inventory.core.enums.TypeMoveStock
import java.math.BigDecimal
import java.time.ZonedDateTime

class StockMovementRequestDTO {
    var date: ZonedDateTime? = null
    var quantity: BigDecimal? = null
    var typeMoveStock: TypeMoveStock? = null
    var sourceStockMovement: SourceStockMovement? = null
}