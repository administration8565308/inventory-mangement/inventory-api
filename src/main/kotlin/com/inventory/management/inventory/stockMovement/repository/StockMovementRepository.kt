package com.inventory.management.inventory.stockMovement.repository

import com.inventory.management.inventory.core.models.StockMovementEntity
import org.springframework.data.jpa.repository.JpaRepository

interface StockMovementRepository: JpaRepository<StockMovementEntity, Int> {
}