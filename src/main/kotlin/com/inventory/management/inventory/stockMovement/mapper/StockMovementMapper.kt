package com.inventory.management.inventory.stockMovement.mapper

import com.inventory.management.inventory.article.mapper.ArticleMapper
import com.inventory.management.inventory.core.models.StockMovementEntity
import com.inventory.management.inventory.stockMovement.dto.request.StockMovementRequestDTO
import com.inventory.management.inventory.stockMovement.dto.response.StockMovementResponseDTO
import java.time.ZonedDateTime

class StockMovementMapper (
    private val articleMapper: ArticleMapper
){

    fun toStockMovementEntity(stockMovementRequestDTO: StockMovementRequestDTO): StockMovementEntity =
        StockMovementEntity().also {
            it.quantity = stockMovementRequestDTO.quantity
            it.type = stockMovementRequestDTO.typeMoveStock
            it.source = stockMovementRequestDTO.sourceStockMovement
            it.creationTime = ZonedDateTime.now()
        }

    fun fromStockMovementDTO(stockMovementEntity: StockMovementEntity): StockMovementResponseDTO =
        StockMovementResponseDTO().also {
            it.id = stockMovementEntity.id
            it.quantity = stockMovementEntity.quantity
            it.typeMoveStock = stockMovementEntity.type
            it.date = stockMovementEntity.date
            it.sourceStockMovement = stockMovementEntity.source
            it.articleDTO = stockMovementEntity.article?.let { article -> articleMapper.fromArticleDto(article) }
            it.creationTime = stockMovementEntity.creationTime
            it.lastModifiedTime = stockMovementEntity.lastModifiedTime
        }
}