package com.inventory.management.inventory.sale.dto

import java.time.ZonedDateTime

class SaleDTO {
    var id: Int? = null
    var code: String? = null
    var date: ZonedDateTime? = null
    var comments: String? = null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}