package com.inventory.management.inventory.sale.mapper

import com.inventory.management.inventory.core.models.SaleEntity
import com.inventory.management.inventory.sale.dto.SaleDTO
import java.time.ZonedDateTime

class SaleMapper {

    fun toSaleEntity(saleDTO: SaleDTO): SaleEntity = SaleEntity().also {
        it.code = saleDTO.code
        it.comments = saleDTO.comments
        it.date = saleDTO.date
        it.creationTime = ZonedDateTime.now()
    }

    fun fromSaleDTO(saleEntity: SaleEntity): SaleDTO = SaleDTO().also {
        it.id = saleEntity.id
        it.code = saleEntity.code
        it.comments = saleEntity.comments
        it.date = saleEntity.date
        it.creationTime = saleEntity.creationTime
        it.lastModifiedTime = saleEntity.lastModifiedTime
    }
}