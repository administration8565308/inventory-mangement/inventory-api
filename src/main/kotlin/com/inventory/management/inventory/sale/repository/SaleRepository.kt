package com.inventory.management.inventory.sale.repository

import com.inventory.management.inventory.core.models.SaleEntity
import org.springframework.data.jpa.repository.JpaRepository

interface SaleRepository: JpaRepository<SaleEntity, Int> {
}