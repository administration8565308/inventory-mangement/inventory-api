package com.inventory.management.inventory

import lombok.Data

@Data
class AddressDTO {
    var address1: String? = null
    var address2: String? = null
    var city: String? = null
    var codePostal: Int = 0
    var country: String? = null
}