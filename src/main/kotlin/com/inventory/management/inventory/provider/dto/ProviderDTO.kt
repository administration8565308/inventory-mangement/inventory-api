package com.inventory.management.inventory.provider.dto

import com.inventory.management.inventory.AddressDTO
import java.time.ZonedDateTime

class ProviderDTO {
    var id: Int? = null
    var firstName: String?= null
    var lastName: String?= null
    var email: String?= null
    var addressDTO: AddressDTO?= null
    var picture: String?= null
    var phone: Int = 0
    var creationTime: ZonedDateTime?= null
    var lastModifiedTime: ZonedDateTime?= null
}