package com.inventory.management.inventory.provider.repository

import com.inventory.management.inventory.core.models.ProviderEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ProviderRepository: JpaRepository<ProviderEntity, Int> {
}