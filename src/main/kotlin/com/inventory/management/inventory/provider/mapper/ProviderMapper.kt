package com.inventory.management.inventory.provider.mapper

import com.inventory.management.inventory.AddressMapper
import com.inventory.management.inventory.core.models.ProviderEntity
import com.inventory.management.inventory.provider.dto.ProviderDTO
import java.time.ZonedDateTime

class ProviderMapper(
    private val addressMapper: AddressMapper
) {

    fun toProviderEntity(providerDTO: ProviderDTO): ProviderEntity = ProviderEntity().also {
        it.firstName = providerDTO.firstName
        it.lastName = providerDTO.lastName
        it.email = providerDTO.email
        it.phone = providerDTO.phone
        it.picture = providerDTO.picture
        it.address = providerDTO.addressDTO?.let { address -> addressMapper.toAddressModel(address) }
        it.creationTime = ZonedDateTime.now()
    }

    fun fromProviderDTO(providerEntity: ProviderEntity): ProviderDTO = ProviderDTO().also {
        it.id = providerEntity.id
        it.firstName = providerEntity.firstName
        it.lastName = providerEntity.lastName
        it.email = providerEntity.email
        it.phone = providerEntity.phone
        it.picture = providerEntity.picture
        it.addressDTO = providerEntity.address?.let { address -> addressMapper.fromAddressModel(address) }
        it.creationTime = providerEntity.creationTime
        it.lastModifiedTime = providerEntity.lastModifiedTime
    }
}