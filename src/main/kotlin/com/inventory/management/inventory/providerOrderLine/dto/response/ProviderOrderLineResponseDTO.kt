package com.inventory.management.inventory.providerOrderLine.dto.response

import com.inventory.management.inventory.article.dto.response.ArticleResponseDTO
import com.inventory.management.inventory.providerOrder.dto.response.ProviderOrderResponseDTO
import java.math.BigDecimal
import java.time.ZonedDateTime

class ProviderOrderLineResponseDTO {
    var id: Int? = null
    var quantity: BigDecimal?= null
    var unitPrice: BigDecimal?= null
    var providerOrderDTO: ProviderOrderResponseDTO?= null
    var articleDTO: ArticleResponseDTO?= null
    var creationTime: ZonedDateTime? = null
    var lastModifiedTime: ZonedDateTime? = null
}