package com.inventory.management.inventory.providerOrderLine.mapper

import com.inventory.management.inventory.article.mapper.ArticleMapper
import com.inventory.management.inventory.core.models.ProviderOrderLineEntity
import com.inventory.management.inventory.providerOrder.mapper.ProviderOrderMapper
import com.inventory.management.inventory.providerOrderLine.dto.request.ProviderOrderLineRequestDTO
import com.inventory.management.inventory.providerOrderLine.dto.response.ProviderOrderLineResponseDTO
import java.time.ZonedDateTime

class ProviderOrderLineMapper(
    private val providerOrderMapper: ProviderOrderMapper,
    private val articleMapper: ArticleMapper
) {
    fun toProviderOrderLineEntity(providerOrderLineRequestDTO: ProviderOrderLineRequestDTO): ProviderOrderLineEntity =
        ProviderOrderLineEntity().also {
            it.quantity = providerOrderLineRequestDTO.quantity
            it.unitPrice = providerOrderLineRequestDTO.unitPrice
            it.creationTime = ZonedDateTime.now()
        }

    fun fromProviderOrderLineDTO(providerOrderLineEntity: ProviderOrderLineEntity): ProviderOrderLineResponseDTO =
        ProviderOrderLineResponseDTO().also {
            it.id = providerOrderLineEntity.id
            it.quantity = providerOrderLineEntity.quantity
            it.unitPrice = providerOrderLineEntity.unitPrice
            it.providerOrderDTO = providerOrderLineEntity.providerOrder?.let { providerOrder ->
                providerOrderMapper.fromProviderOrderDTO(providerOrder)
            }
            it.articleDTO = providerOrderLineEntity.article?.let { article ->
                articleMapper.fromArticleDto(article)
            }
            it.creationTime = providerOrderLineEntity.creationTime
            it.lastModifiedTime = providerOrderLineEntity.lastModifiedTime
        }
}