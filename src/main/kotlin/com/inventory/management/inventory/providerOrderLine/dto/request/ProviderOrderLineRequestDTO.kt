package com.inventory.management.inventory.providerOrderLine.dto.request

import java.math.BigDecimal

class ProviderOrderLineRequestDTO {
    var quantity: BigDecimal?= null
    var unitPrice: BigDecimal?= null
}