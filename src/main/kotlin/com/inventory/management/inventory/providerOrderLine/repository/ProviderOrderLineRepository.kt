package com.inventory.management.inventory.providerOrderLine.repository

import com.inventory.management.inventory.core.models.ProviderOrderLineEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ProviderOrderLineRepository: JpaRepository<ProviderOrderLineEntity, Int> {
}