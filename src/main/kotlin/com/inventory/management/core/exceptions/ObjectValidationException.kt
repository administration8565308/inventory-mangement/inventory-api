package com.inventory.management.core.exceptions


class ObjectValidationException (
    val violations: Set<String>,
    val violationSource: String
) : RuntimeException()