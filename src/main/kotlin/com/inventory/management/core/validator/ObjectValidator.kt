package com.inventory.management.core.validator

import com.inventory.management.core.exceptions.ObjectValidationException
import jakarta.validation.ConstraintViolation
import jakarta.validation.Validation
import jakarta.validation.Validator
import org.springframework.stereotype.Component
import jakarta.validation.ValidatorFactory
import java.util.stream.Collectors

@Component
class ObjectValidator {
    private val factory: ValidatorFactory = Validation.buildDefaultValidatorFactory()
    private val validator: Validator = factory.validator

    fun <T> validate(objectToValidator: T) {
        val violations: Set<ConstraintViolation<T>> = validator.validate(objectToValidator)
        if(violations.isNotEmpty()) {
            val errorMessage: Set<String> = violations.stream()
                .map { it.message }
                .collect(Collectors.toSet())

            throw ObjectValidationException(errorMessage, objectToValidator.toString())
        }
    }

}