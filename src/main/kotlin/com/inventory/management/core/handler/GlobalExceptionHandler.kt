package com.inventory.management.core.handler

import com.inventory.management.core.exceptions.ExceptionRepresentation
import com.inventory.management.core.exceptions.ObjectValidationException
import com.inventory.management.inventory.core.utils.UtilsConstant.OBJECT_EXCEPTION
import com.inventory.management.inventory.core.utils.UtilsConstant.USER_EXITS_EXCEPTION
import jakarta.persistence.EntityNotFoundException
import lombok.extern.slf4j.Slf4j
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.logging.Logger

@RestControllerAdvice
@Slf4j
class GlobalExceptionHandler {

    @ExceptionHandler(ObjectValidationException::class)
    fun handleException(exception: ObjectValidationException): ResponseEntity<ExceptionRepresentation> {
        val representation = ExceptionRepresentation().also {
            it.errorSource = OBJECT_EXCEPTION
            it.errorMessage = exception.violationSource
            it.validationErrors = exception.violations
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(representation)
    }

    @ExceptionHandler(EntityNotFoundException::class)
    fun handleEntityException(exception: EntityNotFoundException): ResponseEntity<ExceptionRepresentation> {
        val representation = ExceptionRepresentation().also {
            it.errorMessage = exception.message
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(representation)
    }

    @ExceptionHandler(DataIntegrityViolationException::class)
    fun handleDataIntegrityException(exception: DataIntegrityViolationException): ResponseEntity<ExceptionRepresentation> {
        val representation = ExceptionRepresentation().also {
            it.errorMessage = USER_EXITS_EXCEPTION
            it.errorSource = exception.message
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(representation)
    }
}