package com.inventory.management.core.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License
import io.swagger.v3.oas.annotations.servers.Server

@OpenAPIDefinition(
    info = Info(
        contact = Contact(
            name = "INVENTORY-MANAGEMENT-API",
            email = "contact@inventorymanagement.com",
            url = "https://zakkeBanking.com/"
        ),
        description = "OpenApi documentation for Spring Security",
        title = "OpenApi specification - INVENTORY-MANAGEMENT-API",
        version= "1.0",
        license = License(
            name = "Licence name",
            url = "https://some-url.com"
        ),
        termsOfService = "Terms of service"
    ),
    servers = [
        Server(
            description = "Local ENV",
            url = "http://localhost:8080"
        ),
        Server(
            description = "PROD ENV",
            url = "https://zakkeBanking.com/"
        )
    ],
)
class OpenApi {
}